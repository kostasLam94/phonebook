﻿using Xunit;

namespace Tests
{
    public class ValidationTests
    {
        [Theory]
        [InlineData("!Kostas")]
        [InlineData("@Kostas")]
        [InlineData("#Kostas")]
        [InlineData("$Kostas")]
        [InlineData("%Kostas")]
        [InlineData("^Kostas")]
        [InlineData("*Kostas")]
        [InlineData("(Kostas")]
        [InlineData(")Kostas")]
        [InlineData("+Kostas")]
        [InlineData("=Kostas")]
        [InlineData("1Kostas")]
        [InlineData("2Kostas")]
        [InlineData("3Kostas")]
        [InlineData("4Kostas")]
        [InlineData("5Kostas")]
        [InlineData("6Kostas")]
        [InlineData("7Kostas")]
        [InlineData("8Kostas")]
        [InlineData("9Kostas")]
        [InlineData("0Kostas")]
        [InlineData(".Kostas")]
        [InlineData("/Kostas")]
        [InlineData("<Kostas")]
        public void NameShouldBeInvalid(string name)
        {
            Assert.False(PhoneBook.Validation.IsValidName(name));
        }


        [Theory]
        [InlineData("6972226475")]
        [InlineData("6911111111")]
        [InlineData("6901234567")]
        [InlineData("6998765432")]
        [InlineData("6900000000")]

        public void MobilePhoneNumberShouldBeValid(string number)
        {
            Assert.True(PhoneBook.Validation.IsValidMobilePhoneNumber(number));
        }

        [Theory]
        [InlineData("69")]
        [InlineData("697222647")]
        [InlineData("6872226475")]
        [InlineData("0000000000")]
        [InlineData("6000000000")]
        [InlineData("")]
        [InlineData("6972226O75)")]
        [InlineData("210829025")]

        public void MobilePhoneNumberShouldBeInvalid(string number)
        {
            Assert.False(PhoneBook.Validation.IsValidMobilePhoneNumber(number));
        }

        [Theory]
        [InlineData("2102829025")]
        [InlineData("2222222222")]
        [InlineData("2121345678")]
        [InlineData("2012134567")]
        [InlineData("2000020000")]

        public void HomePhoneNumberShouldBeValid(string number)
        {
            Assert.True(PhoneBook.Validation.IsValidHomePhoneNumber(number));
        }

        [Theory]
        [InlineData("21082902")]
        [InlineData("22222222")]
        [InlineData("21234567")]
        [InlineData("20123456")]
        [InlineData("20000000")]
        [InlineData("120000000")]
        [InlineData("000000000")]
        [InlineData("6872226475")]
        [InlineData("21O2829125")]

        public void HomePhoneNumberShouldBeInvalid(string number)
        {
            Assert.False(PhoneBook.Validation.IsValidHomePhoneNumber(number));
        }

        [Theory]
        [InlineData("test@gmail.com")]
        [InlineData("a@b.com")]
        [InlineData("a@b.gr")]
        [InlineData(null)]
        [InlineData("")]
        public void EmailAddressShouldBeValid(string email)
        {
            Assert.True(PhoneBook.Validation.IsValidEmail(email));
        }

        [Theory]
        [InlineData("testgmail.com")]
        [InlineData("ab.com")]
        [InlineData("ab.gr")]
        [InlineData("test@gmail.com.")]
        [InlineData("@gmail.com")]
        public void EmailAddressShouldBeInvalid(string email)
        {
            Assert.False(PhoneBook.Validation.IsValidEmail(email));
        }
    }
}