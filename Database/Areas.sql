CREATE TABLE [dbo].[Areas](
	[ZipCode] [varchar](5) NOT NULL,
	[Area] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO