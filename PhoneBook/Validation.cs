﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace PhoneBook
{
    public static class Validation
    {
        public static bool IsValidName(this string name)
        {
            if (name.Any(char.IsDigit) || name.Any(char.IsSymbol) || name.Any(char.IsPunctuation) || string.IsNullOrEmpty(name))
                return false;
            return true;
        }
        public static bool IsValidEmail(this string? email)
        {
            if (string.IsNullOrEmpty(email))
                return true;

            var trimmedEmail = email.Trim();
            if (trimmedEmail.EndsWith("."))
            {
                return false;
            }
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == trimmedEmail;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsValidMobilePhoneNumber(this string phoneNumber)
        {
            return Regex.Match(phoneNumber, @"^69[0-9]{8}$").Success;
        }
        public static bool IsValidHomePhoneNumber(this string? phoneNumber)
        {
            return Regex.Match(phoneNumber, @"^2[0-9]{9}$").Success;
        }
    }
}
