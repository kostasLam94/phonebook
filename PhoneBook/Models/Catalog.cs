﻿using System;
using System.Collections.Generic;

namespace PhoneBook.Models
{
    public partial class Catalog
    {
        public Catalog(string lastName, string firstName, string? emailAddress, string? homePhoneNumber, string mobilePhoneNumber, string zipCode, string area)
        {
            LastName = lastName;
            FirstName = firstName;
            EmailAddress = emailAddress;
            HomePhoneNumber = homePhoneNumber;
            MobilePhoneNumber = mobilePhoneNumber;
            ZipCode = zipCode;
            Area = area;
        }

        public int Id { get; set; }
        public string LastName { get; set; } = null!;
        public string FirstName { get; set; } = null!;
        public string? EmailAddress { get; set; }
        public string? HomePhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; } = null!;
        public string ZipCode { get; set; } = null!;
        public string Area { get; set; } = null!;
    }
}
