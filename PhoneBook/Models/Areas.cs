﻿using System;
using System.Collections.Generic;

namespace PhoneBook.Models
{
    public partial class Areas
    {
        public string ZipCode { get; set; } = null!;
        public string Area { get; set; } = null!;
    }
}
