﻿namespace PhoneBook
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.NewEntryButton = new System.Windows.Forms.Button();
            this.searchLabel = new System.Windows.Forms.Label();
            this.searchString = new System.Windows.Forms.TextBox();
            this.phoneBookDataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.sortByCombo = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.phoneBookDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Location = new System.Drawing.Point(543, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(176, 20);
            this.Title.TabIndex = 0;
            this.Title.Text = "Τηλεφωνικός Κατάλογος";
            // 
            // NewEntryButton
            // 
            this.NewEntryButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NewEntryButton.Location = new System.Drawing.Point(973, 57);
            this.NewEntryButton.Name = "NewEntryButton";
            this.NewEntryButton.Size = new System.Drawing.Size(217, 29);
            this.NewEntryButton.TabIndex = 2;
            this.NewEntryButton.Text = "Προσθήκη Εγγραφής";
            this.NewEntryButton.UseVisualStyleBackColor = true;
            this.NewEntryButton.Click += new System.EventHandler(this.NewEntryButton_Click);
            // 
            // searchLabel
            // 
            this.searchLabel.AutoSize = true;
            this.searchLabel.Location = new System.Drawing.Point(12, 57);
            this.searchLabel.Name = "searchLabel";
            this.searchLabel.Size = new System.Drawing.Size(86, 20);
            this.searchLabel.TabIndex = 3;
            this.searchLabel.Text = "Αναζήτηση";
            // 
            // searchString
            // 
            this.searchString.Location = new System.Drawing.Point(114, 54);
            this.searchString.Name = "searchString";
            this.searchString.Size = new System.Drawing.Size(172, 27);
            this.searchString.TabIndex = 4;
            this.searchString.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // phoneBookDataGridView
            // 
            this.phoneBookDataGridView.AllowUserToAddRows = false;
            this.phoneBookDataGridView.AllowUserToDeleteRows = false;
            this.phoneBookDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phoneBookDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.phoneBookDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.phoneBookDataGridView.Location = new System.Drawing.Point(12, 119);
            this.phoneBookDataGridView.Name = "phoneBookDataGridView";
            this.phoneBookDataGridView.RowHeadersWidth = 51;
            this.phoneBookDataGridView.RowTemplate.Height = 29;
            this.phoneBookDataGridView.Size = new System.Drawing.Size(1178, 401);
            this.phoneBookDataGridView.TabIndex = 1;
            this.phoneBookDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.phoneBookDataGridView_EditingControlShowing);
            this.phoneBookDataGridView.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.phoneBookDataGridView_RowHeaderMouseDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(337, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Ταξινόμηση";
            // 
            // sortByCombo
            // 
            this.sortByCombo.FormattingEnabled = true;
            this.sortByCombo.Location = new System.Drawing.Point(452, 54);
            this.sortByCombo.Name = "sortByCombo";
            this.sortByCombo.Size = new System.Drawing.Size(169, 28);
            this.sortByCombo.TabIndex = 6;
            this.sortByCombo.SelectedIndexChanged += new System.EventHandler(this.sortByCombo_SelectedIndexChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1210, 532);
            this.Controls.Add(this.sortByCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchString);
            this.Controls.Add(this.searchLabel);
            this.Controls.Add(this.NewEntryButton);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.phoneBookDataGridView);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PhoneBook";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.phoneBookDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label Title;
        private Button NewEntryButton;
        private Label searchLabel;
        private DataGridView phoneBookDataGridView;
        private Label label1;
        public TextBox searchString;
        public ComboBox sortByCombo;
    }
}