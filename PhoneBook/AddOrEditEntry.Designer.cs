﻿namespace PhoneBook
{
    partial class AddOrEditEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.firstNameValue = new System.Windows.Forms.TextBox();
            this.lastNameValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mobilePhoneNumberValue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.homePhoneNumberValue = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.confirmButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.zipCodeValue = new System.Windows.Forms.ComboBox();
            this.areaValue = new System.Windows.Forms.ComboBox();
            this.emailValue = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.idValue = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Εισαγωγή/Διόρθωση εγγραφής";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Όνομα*";
            // 
            // firstNameValue
            // 
            this.firstNameValue.Location = new System.Drawing.Point(126, 48);
            this.firstNameValue.Name = "firstNameValue";
            this.firstNameValue.Size = new System.Drawing.Size(150, 27);
            this.firstNameValue.TabIndex = 2;
            // 
            // lastNameValue
            // 
            this.lastNameValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lastNameValue.Location = new System.Drawing.Point(510, 48);
            this.lastNameValue.Name = "lastNameValue";
            this.lastNameValue.Size = new System.Drawing.Size(150, 27);
            this.lastNameValue.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(422, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Επώνυμο*";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(422, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Περιοχή*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(82, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "ΤΚ*";
            // 
            // mobilePhoneNumberValue
            // 
            this.mobilePhoneNumberValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mobilePhoneNumberValue.Location = new System.Drawing.Point(510, 100);
            this.mobilePhoneNumberValue.Name = "mobilePhoneNumberValue";
            this.mobilePhoneNumberValue.Size = new System.Drawing.Size(150, 27);
            this.mobilePhoneNumberValue.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(411, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Κινητό Τηλ*";
            // 
            // homePhoneNumberValue
            // 
            this.homePhoneNumberValue.Location = new System.Drawing.Point(126, 100);
            this.homePhoneNumberValue.Name = "homePhoneNumberValue";
            this.homePhoneNumberValue.Size = new System.Drawing.Size(150, 27);
            this.homePhoneNumberValue.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 20);
            this.label7.TabIndex = 9;
            this.label7.Text = "Σταθερό Τηλ";
            // 
            // confirmButton
            // 
            this.confirmButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.confirmButton.Location = new System.Drawing.Point(190, 315);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(121, 29);
            this.confirmButton.TabIndex = 13;
            this.confirmButton.Text = "Επιβεβαίωση";
            this.confirmButton.UseVisualStyleBackColor = true;
            this.confirmButton.Click += new System.EventHandler(this.confirmButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.Location = new System.Drawing.Point(432, 315);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(94, 29);
            this.cancelButton.TabIndex = 14;
            this.cancelButton.Text = "Ακύρωση";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // zipCodeValue
            // 
            this.zipCodeValue.FormattingEnabled = true;
            this.zipCodeValue.Location = new System.Drawing.Point(126, 149);
            this.zipCodeValue.Name = "zipCodeValue";
            this.zipCodeValue.Size = new System.Drawing.Size(151, 28);
            this.zipCodeValue.TabIndex = 15;
            this.zipCodeValue.Validated += new System.EventHandler(this.zipCodeValue_Validated);
            // 
            // areaValue
            // 
            this.areaValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.areaValue.FormattingEnabled = true;
            this.areaValue.Location = new System.Drawing.Point(510, 149);
            this.areaValue.Name = "areaValue";
            this.areaValue.Size = new System.Drawing.Size(151, 28);
            this.areaValue.TabIndex = 16;
            this.areaValue.Validated += new System.EventHandler(this.areaValue_Validated);
            // 
            // emailValue
            // 
            this.emailValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.emailValue.Location = new System.Drawing.Point(511, 208);
            this.emailValue.Name = "emailValue";
            this.emailValue.Size = new System.Drawing.Size(150, 27);
            this.emailValue.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(287, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(207, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "Διεύθυνση Ηλ. Ταχυδρομείου";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 357);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(234, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "Τα πεδία με \'*\' είναι υποχρεωτικά";
            // 
            // idValue
            // 
            this.idValue.Enabled = false;
            this.idValue.Location = new System.Drawing.Point(242, 2);
            this.idValue.Name = "idValue";
            this.idValue.Size = new System.Drawing.Size(35, 27);
            this.idValue.TabIndex = 20;
            this.idValue.Visible = false;
            // 
            // AddOrEditEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 387);
            this.Controls.Add(this.idValue);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.emailValue);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.areaValue);
            this.Controls.Add(this.zipCodeValue);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.confirmButton);
            this.Controls.Add(this.mobilePhoneNumberValue);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.homePhoneNumberValue);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lastNameValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.firstNameValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddOrEditEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AddOrEditEntry_FormClosed);
            this.Load += new System.EventHandler(this.NewEntry_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Button confirmButton;
        private Button cancelButton;
        private Label label8;
        private Label label9;
        public TextBox firstNameValue;
        public TextBox lastNameValue;
        public TextBox mobilePhoneNumberValue;
        public TextBox homePhoneNumberValue;
        public ComboBox zipCodeValue;
        public ComboBox areaValue;
        public TextBox emailValue;
        public TextBox idValue;
        public Label label1;
    }
}