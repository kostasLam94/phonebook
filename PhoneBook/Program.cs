﻿namespace PhoneBook
{
    //Η εφαρμογή επικοινωνεί με τη βάση δεδομένων μέσω του EntityFrameworkCore
    internal static class Program
    {
        [STAThread]
        static void Main()
        {
            ApplicationConfiguration.Initialize();
            Application.Run(new MainForm());
        }
    }
}