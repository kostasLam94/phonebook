﻿using System.Collections;
using System.Data;
using PhoneBook.Models;

namespace PhoneBook
{
    public partial class MainForm : Form
    {
        ComboBox combo;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //Προσθήκη Valid Values Ταξινόμησης. Δεν εχουν μπει ως φίλτρα τα τηλέφωνα. 
            AddComboBoxValues();
            //Επικοινωνία με τη βάση δεδομένων μέσω του EntityFrameworkCore για τη συμπλήρωση των δεδομένων στον πίνακα
            LoadGridData(null, null);
        }

        private void NewEntryButton_Click(object sender, EventArgs e)
        {
            AddOrEditEntry newEntry = new AddOrEditEntry();
            newEntry.Text = "Εισαγωγή Εγγραφής";
            newEntry.label1.Text = newEntry.Text;
            newEntry.Show();
            this.Hide();
        }

        private void phoneBookDataGridView_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int id = (int)phoneBookDataGridView.Rows[e.RowIndex].Cells["Id"].Value;
            EditRowEntry(id);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           LoadGridData(this.searchString.Text, this.sortByCombo.Text);
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void AddComboBoxValues()
        {
            this.sortByCombo.Items.Add("Όνομα - Αύξουσα");
            this.sortByCombo.Items.Add("Όνομα - Φθίνουσα");
            this.sortByCombo.Items.Add("Επώνυμο - Αύξουσα");
            this.sortByCombo.Items.Add("Επώνυμο - Φθίνουσα");
            this.sortByCombo.Items.Add("Εmail - Αύξουσα");
            this.sortByCombo.Items.Add("Εmail - Φθίνουσα");
            this.sortByCombo.Items.Add("ΤΚ - Αύξουσα");
            this.sortByCombo.Items.Add("ΤΚ - Φθίνουσα");
            this.sortByCombo.Items.Add("Περιοχή - Αύξουσα");
            this.sortByCombo.Items.Add("Περιοχή - Φθίνουσα");
            this.sortByCombo.Items.Add("Χωρίς Φίλτρο");

            this.sortByCombo.SelectedIndex = 10;
        }

        internal void LoadGridData(string? searchString, string? sortFilter)
        {
            using (var context = new PhoneBookContext())
            {
                //εισαγωγή όλων των εγγραφών του πίνακα Catalogs
                var entries = context.Catalogs.Select(x => new { x.Id, Όνομα = x.FirstName, Επώνυμο = x.LastName, Email = x.EmailAddress, Σταθερό_Τηλ = x.HomePhoneNumber, Κινητό_Τηλ = x.MobilePhoneNumber, ΤΚ = x.ZipCode, Περιοχή = x.Area }).ToList();
                
                //Φιλτράρισμα των εγγραφών με βάση την αναζήτηση
                if (!string.IsNullOrEmpty(searchString))
                {
                    searchString = searchString.ToLower();
                    entries = entries.Where(x => x.Επώνυμο.ToLower().Contains(searchString) ||
                    x.Όνομα.ToLower().Contains(searchString) ||
                    x.Email.ToLower().Contains(searchString) ||
                    x.Κινητό_Τηλ.ToLower().Contains(searchString) ||
                    x.Σταθερό_Τηλ.ToLower().Contains(searchString) ||
                    x.Περιοχή.ToLower().Contains(searchString) ||
                    x.ΤΚ.ToLower().Contains(searchString))
                        .ToList();
                }

                //Ταξινόμηση των εγγραφών
                if (!string.IsNullOrEmpty(sortFilter))
                {
                    switch (sortFilter)
                    {
                        case "Όνομα - Αύξουσα":
                            entries = entries.OrderBy(x => x.Όνομα).ToList();
                            break;
                        case "Όνομα - Φθίνουσα":
                            entries = entries.OrderByDescending(x => x.Όνομα).ToList();
                            break;
                        case "Επώνυμο - Αύξουσα":
                            entries = entries.OrderBy(x => x.Επώνυμο).ToList();
                            break;
                        case "Επώνυμο - Φθίνουσα":
                            entries = entries.OrderByDescending(x => x.Επώνυμο).ToList();
                            break;
                        case "Εmail - Αύξουσα":
                            entries = entries.OrderBy(x => x.Email).ToList();
                            break;
                        case "Εmail - Φθίνουσα":
                            entries = entries.OrderByDescending(x => x.Email).ToList();
                            break;
                        case "ΤΚ - Αύξουσα":
                            entries = entries.OrderBy(x => x.ΤΚ).ToList();
                            break;
                        case "ΤΚ - Φθίνουσα":
                            entries = entries.OrderByDescending(x => x.ΤΚ).ToList();
                            break;
                        case "Περιοχή - Αύξουσα":
                            entries = entries.OrderBy(x => x.Περιοχή).ToList();
                            break;
                        case "Περιοχή - Φθίνουσα":
                            entries = entries.OrderByDescending(x => x.Περιοχή).ToList();
                            break;
                        default:
                            break;
                    }
                }

                phoneBookDataGridView.DataSource = entries;

                if (!phoneBookDataGridView.Columns.Contains("Options"))
                {
                    DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn();
                    combo.Name = "Options";
                    combo.HeaderText = "Επιλογές";

                    combo.Items.Add("Επεξεργασία");
                    combo.Items.Add("Διαγραφή");
                    //combo.Items.Add("Ταξινόμηση");

                    phoneBookDataGridView.Columns.Add(combo);
                }

                this.phoneBookDataGridView.Columns["Id"].Visible = false;


                foreach (DataGridViewColumn column in phoneBookDataGridView.Columns)
                {
                    column.SortMode = DataGridViewColumnSortMode.Automatic;
                } 
            }
        }

        private void EditRowEntry(int id)
        {
            using (PhoneBookContext context = new PhoneBookContext())
            {
                //Εύρεση της εγγραφής με βάση το Id της
                var entry = context.Catalogs.Find(id)!;

                AddOrEditEntry editEntry = new AddOrEditEntry();
                editEntry.Text = "Διόρθωση Εγγραφής";
                editEntry.label1.Text = editEntry.Text;
                editEntry.idValue.Text = entry.Id.ToString();
                editEntry.firstNameValue.Text = entry.FirstName;
                editEntry.lastNameValue.Text = entry.LastName;
                editEntry.homePhoneNumberValue.Text = entry.HomePhoneNumber;
                editEntry.mobilePhoneNumberValue.Text = entry.MobilePhoneNumber;
                editEntry.emailValue.Text = entry.EmailAddress;
                editEntry.zipCodeValue.Text = entry.ZipCode;
                editEntry.areaValue.Text = entry.Area;

                
                editEntry.Show();
                this.Hide();
            }
        }

        private void DeleteRowEntry(int id)
        {
            //Διαγραφή της εγγραφής μέσω του Entity Framework Core
            using(PhoneBookContext context = new PhoneBookContext())
            {
                if (context.Database.CurrentTransaction == null)
                    context.Database.BeginTransaction();

                try
                {
                    var entry = context.Catalogs.Find(id)!;
                    context.Catalogs.Remove(entry);
                    context.SaveChanges();

                    if (context.Database.CurrentTransaction != null)
                        context.Database.CommitTransaction();
                }
                catch (Exception e)
                {
                    if (context.Database.CurrentTransaction != null)
                        context.Database.RollbackTransaction();
                    MessageBox.Show(e.Message);
                }
            }
            //Ανανέωση των δεδομένων μετά τη διαγραφή
            LoadGridData(this.searchString.Text, this.sortByCombo.Text);
        }

        private void sortByCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Ταξινόμηση των δεδομένων
            LoadGridData(this.searchString.Text, this.sortByCombo.Text);
        }

        private void phoneBookDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            combo = e.Control as ComboBox;
            if (combo != null)
            {
                combo.SelectedIndexChanged -= new EventHandler(Combo_SelectedIndexChanged);

                combo.SelectedIndexChanged += Combo_SelectedIndexChanged;
            }
        }

        private void Combo_SelectedIndexChanged(object? sender, EventArgs e)
        {
            string comboValue = (sender as ComboBox).SelectedItem.ToString();
            int id = int.Parse(phoneBookDataGridView.Rows[phoneBookDataGridView.CurrentCell.RowIndex].Cells[1].Value.ToString());

            switch (comboValue)
            {
                case "Επεξεργασία":
                    EditRowEntry(id);
                    break;
                case "Διαγραφή":
                    if (MessageBox.Show("Επιβεβαίωση διαγραφής της εγγραφής;", "Διαγραφή εγγραφής", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        DeleteRowEntry(id);
                    break;
                default:
                    break;
            }
        }
    }
}