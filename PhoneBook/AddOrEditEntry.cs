﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PhoneBook.Models;
using PhoneBook;
using Microsoft.EntityFrameworkCore;

namespace PhoneBook
{
    public partial class AddOrEditEntry : Form
    {
        public AddOrEditEntry()
        {
            InitializeComponent();
        }

        private void NewEntry_Load(object sender, EventArgs e)
        {
            using (PhoneBookContext context = new PhoneBookContext())
            {
                List<Areas> areas = context.Areas.ToList();

                foreach (var line in areas)
                {
                    this.zipCodeValue.Items.Add(line.ZipCode);
                    this.areaValue.Items.Add(line.Area);
                }
            }
        }

        private void confirmButton_Click(object sender, EventArgs e)
        {
            Catalog entry = new Catalog(this.lastNameValue.Text,
                        this.firstNameValue.Text,
                        this.emailValue.Text,
                        this.homePhoneNumberValue.Text,
                        this.mobilePhoneNumberValue.Text,
                        this.zipCodeValue.Text,
                        this.areaValue.Text);

            //Επικύρωση των δεδομένων
            ValidateEntry(entry);

            using (PhoneBookContext context = new PhoneBookContext())
            {
                if (context.Database.CurrentTransaction == null)
                    context.Database.BeginTransaction();

                try
                {
                    //Αν το Id ειναι είναι κενό και υπάρχει στην βάση τοτε ενημέρωση της εγγραφής
                    if (!string.IsNullOrEmpty(this.idValue.Text) && context.Catalogs.Any(x => x.Id == int.Parse(this.idValue.Text)))
                    {
                        var entryToUpdate = context.Catalogs.Find(int.Parse(this.idValue.Text))!;

                        entryToUpdate.LastName = entry.LastName;
                        entryToUpdate.FirstName = entry.FirstName;
                        entryToUpdate.EmailAddress = entry.EmailAddress;
                        entryToUpdate.HomePhoneNumber = entry.HomePhoneNumber;
                        entryToUpdate.MobilePhoneNumber = entry.MobilePhoneNumber;
                        entryToUpdate.ZipCode = entry.ZipCode;
                        entryToUpdate.Area = entry.Area;

                        context.Update(entryToUpdate);

                        if (MessageBox.Show("Επιβεβαίωση ενημέρωσης της εγγραφής;", "Eνημέρωση εγγραφής", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            context.SaveChanges();
                            if (context.Database.CurrentTransaction != null)
                                context.Database.CommitTransaction();
                            MessageBox.Show("Η εγγραφή ενημερώθηκε επιτυχώς");

                            
                            MainForm mainForm = new MainForm();
                            mainForm.LoadGridData(null, null);
                            mainForm.Show();
                            this.Hide();
                        }
                    }
                    else
                    {
                        //Διαφορετικά καταχώρηση νέας εγγραφής
                        context.Catalogs.Add(entry);

                        if (MessageBox.Show("Επιβεβαίωση καταχώρησης νέας εγγραφής;", "Καταχώρηση εγγραφής", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            context.SaveChanges();
                            if(context.Database.CurrentTransaction != null)
                                context.Database.CommitTransaction();
                            MessageBox.Show("Η εγραφή καταχωρήθηκε επιτυχώς.");

                            
                            MainForm mainForm = new MainForm();
                            mainForm.LoadGridData(null, null);
                            mainForm.Show();
                            this.Hide();
                        }
                    }

                }
                catch (Exception ex)
                {
                    if (context.Database.CurrentTransaction != null)
                        context.Database.RollbackTransaction();
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            MainForm mainForm = new MainForm();
            mainForm.Show();
            this.Hide();
        }

        private void zipCodeValue_Validated(object sender, EventArgs e)
        {
            using (PhoneBookContext context = new PhoneBookContext())
            {
                List<Areas> areas = context.Areas.Where(x => x.ZipCode == this.zipCodeValue.Text).ToList();
                if (areas.Count == 0)
                    return;
                this.areaValue.Items.Clear();
                foreach (var line in areas)
                {
                    this.areaValue.Items.Add(line.Area);
                    this.areaValue.SelectedItem = line.Area;
                }
            }
        }

        private void areaValue_Validated(object sender, EventArgs e)
        {
            using (PhoneBookContext context = new PhoneBookContext())
            {
                List<Areas> areas = context.Areas.Where(x => x.Area == this.areaValue.Text).ToList();
                foreach (var line in areas)
                {
                    this.zipCodeValue.SelectedItem = line.ZipCode;
                    break;
                }
            }
        }

        private void AddOrEditEntry_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainForm mainForm = new MainForm();
            mainForm.Show();
        }

        private void ValidateEntry(Catalog entry)
        {
            if (!entry.FirstName.IsValidName())
                throw new Exception("Η εισαγωγή ονόματος δεν είναι έγκυρη.");
            if (!entry.LastName.IsValidName())
                throw new Exception("Η εισαγωγή επωνύμου δεν είναι έγκυρη.");
            if (!string.IsNullOrEmpty(entry.HomePhoneNumber) && !entry.HomePhoneNumber.Replace("-", null).IsValidHomePhoneNumber())
                throw new Exception("Ο αριθμός σταθερού τηλεφώνου δεν είναι έγκυρος");
            if (!entry.MobilePhoneNumber.Replace("-", null).IsValidMobilePhoneNumber())
                throw new Exception("Ο αριθμός κινητού τηλεφώνου δεν είναι έγκυρος");
            if (!string.IsNullOrEmpty(entry.EmailAddress) && !entry.EmailAddress.IsValidEmail())
                throw new Exception("Η Διεύθυνση Ηλεκτρονικού ταχυδρομείου δεν είναι έγκυρη.");
        }
    }
}
